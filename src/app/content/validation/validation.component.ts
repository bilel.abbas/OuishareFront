import { Component, OnInit } from '@angular/core';
import { Device } from 'src/app/models/device';
import { RentalDtoCreate } from 'src/app/models/rental-dto-create';
import { RentalService } from 'src/app/services/rental.service';
import { DeviceIdService } from 'src/app/services/device-id.service';
import { ActivatedRoute } from '@angular/router';
import { RentalDto } from 'src/app/models/rental-dto';
import { RentalComponent } from '../rental/rental.component';

declare function globalConfig(): any;

@Component({
  selector: 'app-validation',
  templateUrl: './validation.component.html',
  styleUrls: ['./validation.component.css']
})
export class ValidationComponent implements OnInit {
  entityD: Device = new Device();
  entityR: RentalDtoCreate = new RentalDtoCreate();
  rental: RentalDto = new RentalDto();
  openCode: string;
  numeroCasier: string;
  delaiSignalement: string;

  constructor(
    private RentalServ: RentalService,
    private DeviceServ: DeviceIdService,
    private route: ActivatedRoute,
    // private rentalC:RentalComponent,
  ) { }

  ngOnInit() {
    globalConfig();
    const id = +this.route.snapshot.paramMap.get('id');
    this.RentalServ.readById(id).subscribe(
      (returned: RentalDto) => {
        this.rental = returned;
        this.prepareDevice();
      }
    );
  }

  prepareDevice() {
      this.DeviceServ.searchById(this.rental.device.id).subscribe( 
        (returned: Device) => {
          this.entityD = returned;
        }
      );
    }

  prepareOpenCode() {
    this.openCode = "Le code défini par le logiciel d'ouverture des casiers"
  }

  prepareNumeroCasier() {
    this.numeroCasier = "Le numéro du casier récupéré par la future méthode de récupération du numéro de casier en partant du device"
  }



}
