import { Component, OnInit } from '@angular/core';

declare function globalConfig(): any;

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    globalConfig();
  }

}
