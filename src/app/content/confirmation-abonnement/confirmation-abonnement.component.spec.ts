import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirmationAbonnementComponent } from './confirmation-abonnement.component';

describe('ConfirmationAbonnementComponent', () => {
  let component: ConfirmationAbonnementComponent;
  let fixture: ComponentFixture<ConfirmationAbonnementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmationAbonnementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationAbonnementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
