import { Component, OnInit } from '@angular/core';
import { Device } from 'src/app/models/device';
import {RentalService} from '../../services/rental.service';
import { RentalDtoUpdate } from '../../models/rental-dto-update';
import { DeviceIdService } from '../../services/device-id.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-confirmation-abonnement',
  templateUrl: './confirmation-abonnement.component.html',
  styleUrls: ['./confirmation-abonnement.component.css']
})
export class ConfirmationAbonnementComponent implements OnInit {

  idDevice: number
  
  rental: RentalDtoUpdate

  device: Device = new Device();

  constructor(private RentalServ: RentalService, private DeviceServ: DeviceIdService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.idDevice = +this.route.snapshot.paramMap.get('id');
    this.DeviceServ.searchById(this.idDevice).subscribe(
      (device : Device) => {
        this.device = device;
      }

    );


  }
  

}
