import { Component, OnInit } from '@angular/core';

declare function globalConfig(): any;

@Component({
  selector: 'app-our-services',
  templateUrl: './our-services.component.html',
  styleUrls: ['./our-services.component.css']
})
export class OurServicesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    globalConfig();
  }

}
