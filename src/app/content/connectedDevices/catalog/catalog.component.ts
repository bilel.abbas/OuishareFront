import { Component, OnInit } from '@angular/core';
import { ConnectedDevicesDto } from 'src/app/models/connected-devices-dto';
import { ConnectedDevicesService } from 'src/app/services/ConnectedDevicesService-service.service';

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.css']
})
export class CatalogComponent implements OnInit {

  listCatalog:Array<ConnectedDevicesDto>=[];
  emptyList:boolean;

  constructor(private service:ConnectedDevicesService) {}

  ngOnInit() {

    this.readAllCatalog();



  }

  readAllCatalog(){

      this.service.readAllCatalog().subscribe(  
        (x: Array<ConnectedDevicesDto>) => {
        this.listCatalog = x;
          if(x.length==0){
            this.emptyList=true;
          }else{
            this.emptyList=false;
          }

      });
  }


}
