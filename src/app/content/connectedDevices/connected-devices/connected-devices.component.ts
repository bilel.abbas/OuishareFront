import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare function globalConfig(): any;


@Component({
  selector: 'app-connected-devices',
  templateUrl: './connected-devices.component.html',
  styleUrls: ['./connected-devices.component.css']
})
export class ConnectedDevicesComponent implements OnInit {


  constructor(public router: Router) { }

  ngOnInit() {
    globalConfig();
    this.isConnected();

  }


  isConnected(){

    if(localStorage.getItem('isLogged')!='true'){
      this.router.navigateByUrl('/');
    }

  }

}
