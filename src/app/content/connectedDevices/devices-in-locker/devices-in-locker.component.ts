import { Component, OnInit } from '@angular/core';
import { ConnectedDevicesDto } from '../../../models/connected-devices-dto';
import { ConnectedDevicesService } from '../../../services/ConnectedDevicesService-service.service';
import { ConnectedCustomerWithIdDto } from 'src/app/models/connected-customer-with-id-dto';


@Component({
  selector: 'app-devices-in-locker',
  templateUrl: './devices-in-locker.component.html',
  styleUrls: ['./devices-in-locker.component.css']
})
export class DevicesInLockerComponent implements OnInit {

  listInCustomer:Array<ConnectedDevicesDto>=[];
  emptyList:boolean;
  customerEmail:string='';


  constructor(private service:ConnectedDevicesService) {}

  ngOnInit() {
    this.refresh();

  }

  refresh(){

    var obj = JSON.parse(localStorage.getItem('loggedCustomer'));

    this.service.readAllInCustomerFromCustomerId(String(obj.id)).subscribe(
      (x: Array<ConnectedDevicesDto>) => {
        this.checkAvailability(x);
        this.listInCustomer = x;
        if(x.length==0){
          this.emptyList=true;
        }else{
          this.emptyList=false;
        }
      });
  }
  checkAvailability(list:Array<ConnectedDevicesDto>){

    list.forEach(element=>{
    this.service.isAvailableNow(element.id).subscribe(
        (x:boolean)=>{
          element.availableNow=x;
        }
      );
      }
    );
  }

}
