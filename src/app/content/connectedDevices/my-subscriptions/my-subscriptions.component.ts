import { Component, OnInit } from '@angular/core';
import { ConnectedDevicesService } from 'src/app/services/ConnectedDevicesService-service.service';
import { ConnectedDevicesDto } from 'src/app/models/connected-devices-dto';
import { ConnectedCustomerWithIdDto } from 'src/app/models/connected-customer-with-id-dto';

@Component({
  selector: 'app-my-subscriptions',
  templateUrl: './my-subscriptions.component.html',
  styleUrls: ['./my-subscriptions.component.css']
})
export class MySubscriptionsComponent implements OnInit {

  customerEmail:string='';
  listOfSubscribed:Array<ConnectedDevicesDto>
  emptyList:boolean;

  constructor(private service:ConnectedDevicesService) { }

  ngOnInit() {
    this.refresh();
  }

  refresh(){
    
    var obj = JSON.parse(localStorage.getItem('loggedCustomer'));

    this.service.readAllFromSubscribedFromCustomerId(String(obj.id)).subscribe(
      (x: Array<ConnectedDevicesDto>) => {
        this.checkAvailability(x);
        this.listOfSubscribed = x;
        if(x.length==0){
          this.emptyList=true;
        }else{
          this.emptyList=false;
        }

      });

  }

  checkAvailability(list:Array<ConnectedDevicesDto>){

    list.forEach(element=>{
    this.service.isAvailableNow(element.id).subscribe(
        (x:boolean)=>{
          console.log('coucou');
          element.availableNow=x;
        }
      );
      }
    );
  }
}
