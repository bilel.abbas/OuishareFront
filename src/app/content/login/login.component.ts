import { Component, OnInit } from '@angular/core';
import { CustomerDtoLogin } from 'src/app/models/customer-dto-login';
import { CustomerService } from 'src/app/services/customer.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { CustomerDtoUpdate } from 'src/app/models/customer-dto-update';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  entity: CustomerDtoLogin = new CustomerDtoLogin();
  customer: CustomerDtoUpdate = new CustomerDtoUpdate();
  message: string;
  messageColor: string;
  formulaire: FormGroup;

  constructor(private service: CustomerService, public router: Router) { }

  ngOnInit() {
    this.formulaire = new FormGroup({
      email: new FormControl('', Validators.required),
      pwd: new FormControl('', Validators.required)
    });
    if (localStorage.getItem('isLogged') == 'true') {
      this.router.navigateByUrl('/client/connected-devices');
    }
  }

  annuler() {
    this.entity.email = "";
    this.entity.pwd = "";
  }

  login() {
    console.log('DEBUG LOGIN INPUTS : ', this.entity)
    this.service.login(this.entity).subscribe(
      (resp) => {
        console.log('DEBUG LOGIN RESPONSE : ', resp)
        if (resp != null) {
          this.messageColor = 'green';
          this.message = 'Le client dont l\'email est ' + this.entity.email + ' est correct';
          localStorage.setItem('isLogged', 'true');
          this.service.findByEmail(this.entity.email).subscribe(
            (success: CustomerDtoUpdate) => {
              this.customer = success;
              localStorage.setItem('loggedCustomer', JSON.stringify(this.customer));
            }
          )
          location.reload();
        } else {
          this.messageColor = 'red';
          this.message = 'Le client n\'existe pas!';
        }

      },
      (err) => {
        this.messageColor = 'red';
        this.message = 'Le client n\'existe pas!';
        console.log('DEBUG ERROR : ', err);


        if (err.error.status === 400) {
          this.message += ' Les champs ne sont pas valides!';
        }


        /*      if(this.entity.email ==null || this.entity.pwd==null) {
              if((this.entity.email==null) && (this.entity.pwd==null)){     
                this.message ='Veuillez mettre un email et un mot de passe';
              }
          
              if((this.entity.email==null) && (this.entity.pwd!=null)){   
              (err) => {
                this.messageColor = 'red';
                this.message = 'Le client n\'existe pas!';
                console.log('DEBUG ERROR : ', err);
        
        
                if (err.error.status === 400) {
                  this.message += ' Les champs ne sont pas valides!';
                }
        
        
                if (this.entity.email == null || this.entity.pwd == null) {
                  if ((this.entity.email == null) && (this.entity.pwd == null)) {
                    this.message = 'Veuillez mettre un email et un mot de passe';
                  }
        
                  if ((this.entity.email == null) && (this.entity.pwd != null)) {
        
                    this.message = 'Veuillez mettre un email';
                  }
        
                  if ((this.entity.email != null) && (this.entity.pwd == null)) {
        
                    this.message = 'Veuillez mettre un mot de passe';
                  }
                }
              }
            } */
      }
    );
  }
}

