import { Component, ChangeDetectionStrategy, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import {
  CalendarDateFormatter,
  CalendarEvent,
  CalendarWeekViewBeforeRenderEvent,
  CalendarView,
  DAYS_OF_WEEK
} from 'angular-calendar';
import { CustomDateFormatter } from './custom-date-formatter.provider';
import { RentalService } from '../../../services/rental.service';
import { RentalDtoAvail } from '../../../models/rental-dto-avail';
import { Subject } from 'rxjs';
import { RentalComponent } from '../rental.component';
import { Device } from '../../../models/device';

@Component({
  selector: 'mwl-demo-component',
  changeDetection: ChangeDetectionStrategy.Default,
  templateUrl: 'calendar.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: CalendarDateFormatter,
      useClass: CustomDateFormatter
    }
  ],
  styles: [
    `
      .bg-pink {
        background-color: LightCoral !important;
      }
      .bg-green {
        background-color: LightGreen !important;
      }
    `
  ]
})
export class CalendarComponent {

  constructor(private rentalService: RentalService, private rental: RentalComponent) { }


  startD: Number;
  endD: Number;
  duration: number;
  startDD: Number;
  endDD: Number;
  listAvail: Array<RentalDtoAvail> = [];
  click: number;
  refresh: Subject<any> = new Subject();
  now: Date;
  @Input() entityD: Device;
  @Input() prixEstimer: number;
  clickedDate: Date;
  clickedDateValid: Date;
  dateValid = true;
  @Output() outputDate = new EventEmitter<Date>();

  view: CalendarView = CalendarView.Week;
  viewDate = new Date();
  events: CalendarEvent[] = [];
  locale: string = 'fr';
  weekStartsOn: number = DAYS_OF_WEEK.MONDAY;
  weekendDays: number[] = [DAYS_OF_WEEK.SUNDAY, DAYS_OF_WEEK.SATURDAY];
  CalendarView = CalendarView;

  beforeWeekViewRender(renderEvent: CalendarWeekViewBeforeRenderEvent) {
    // console.log('LAAAAAAAAAAAAAAAAAAAA->', this.entityD);

    this.rentalService.availability(this.entityD.id).subscribe(
      (i: Array<RentalDtoAvail>) => {
        this.listAvail = i;
        this.duration = this.entityD.rentalDurationInHours;
        // console.log('DEBUG : FETCHED DATA : ', this.listAvail)
        this.listAvail.forEach(element => {
          this.startD = new Date(element.startDate).getTime();
          this.endD = new Date(element.endDateTheoritical).getTime();
          this.click = new Date(this.clickedDateValid).getTime();
          this.now = new Date();

          renderEvent.hourColumns.forEach(hourColumn => {
            hourColumn.hours.forEach(hour => {
              hour.segments.forEach(segment => {
                if (
                  segment.date.getTime() >= this.startD
                  &&
                  segment.date.getTime() < this.endD
                ) {
                  segment.cssClass = 'bg-pink';
                }
                if (
                  segment.date.getTime() >= this.click
                  &&
                  segment.date.getTime() < (this.click + (this.duration * 3600000 * (this.prixEstimer + 1)))
                ) {
                  segment.cssClass = 'bg-green';
                }
              });
            });
          });

        })
      }

    );
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  verifDate(date: Date) {
    this.listAvail.forEach(element => {
      this.startDD = new Date(element.startDate).getTime();
      this.endDD = new Date(element.endDateTheoritical).getTime();
      this.now = new Date();
      if (
        this.verifVerifDate(date)
      ) {
        // console.log('NOPE')
        this.dateValid = false;
      }
    })
    if (this.dateValid == true) {
      this.clickedDateValid = date;
      this.outputDate.emit(new Date(date));
      // console.log('YEP')
    } else {
      this.dateValid = true;
    }
    this.refresh.next();

  }

  verifVerifDate(date: Date): boolean {
    for (let i = 0; i <= this.duration + (this.duration * (this.prixEstimer)); i++) {
      if (
        (date.getTime() >= this.startDD
          &&
          date.getTime() <= this.endDD)
        ||
        ((date.getTime() + (i * 3600000)) >= this.startDD
          &&
          (date.getTime() + (i * 3600000)) <= this.endDD)
        ||
        date.getTime() <= (this.now.getTime() - (this.duration * 3600000) + 900000)

      ) {
        return true;
      }

    } return false;

  }

}
