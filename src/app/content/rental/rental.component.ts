import { Component, OnInit } from '@angular/core';
import { Device } from '../../models/device'
import { DeviceIdService } from '../../services/device-id.service'
import { ActivatedRoute } from '@angular/router'
import { RentalService } from '../../services/rental.service';
import { RentalDtoCreate } from '../../models/rental-dto-create';
import { RentalDtoUpdate } from '../../models/rental-dto-update';
import { RentalDto } from '../../models/rental-dto';
import { RentalDtoAvail } from '../../models/rental-dto-avail';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';

declare function globalConfig(): any;
@Component({
  selector: 'app-rental',
  templateUrl: './rental.component.html',
  styleUrls: ['./rental.component.css'],
  providers: [DatePipe]
})
export class RentalComponent implements OnInit {

  id: number;
  idEnd: number;
  prix: number = 0;
  idDevice: number;
  idRental: number;
  idReview: number;
  prixEstimer: number = 0;
  messageValidation: string;
  entityD: Device = new Device();
  entity: RentalDto = new RentalDto();
  listRentalAll: Array<RentalDto> = [];
  listAvail: Array<RentalDtoAvail> = [];
  listRentalReal: Array<RentalDto> = [];
  listRentalDelete: Array<RentalDto> = [];
  entityA: RentalDtoAvail = new RentalDtoAvail();
  entityC: RentalDtoCreate = new RentalDtoCreate();
  entityU: RentalDtoUpdate = new RentalDtoUpdate();
  bordel: any;
  clickedDateR: Date;
  endDate: Date = new Date();

  constructor(
    private rentalService: RentalService,
    private serviceD: DeviceIdService,
    private route: ActivatedRoute,
    private datePipe: DatePipe,
    private router: Router,
    ) { }

  ngOnInit() {
    globalConfig();
    const id = +this.route.snapshot.paramMap.get('id');
    this.search(id);
  }

  search(id: number) {
    this.serviceD.searchById(id).subscribe(
      (returned: Device) => {
        this.entityD = returned;
        this.calcul();
        this.bordel = 1;
      }
    );
  }

  calcul() {
    this.prix = this.entityD.pricePonctual + (this.prixEstimer * this.entityD.pricePonctual);
    (returned: number) => {
      this.prix = returned;
    }
  }

  date(dateClick: Date) {
    this.clickedDateR = dateClick;
    // console.log("CA MARCHE ====> ", this.clickedDateR);
    let a = (dateClick.getTime() + (this.entityD.rentalDurationInHours * 3600000 * (this.prixEstimer + 1)));
    // this.endDate.setTime(a);
    this.endDate = new Date(a);
    // console.log('DEBUG this.endDate : ' + this.endDate)
    this.entityC.startDate = this.datePipe.transform(dateClick, 'yyyy-MM-ddTHH:mm:ss');
    this.entityC.endDateTheoritical = this.datePipe.transform(this.endDate, 'yyyy-MM-ddTHH:mm:ss');
    this.entityC.idDevice = this.entityD.id;
    this.entityC.idCustomer = 1;
    this.entityC.subscription = false;
    // console.log('bdioqfguizefguizdqfgvyudzpfgyuepfbuipezf', this.entityC);
  }

  create() {
    // console.log('DEBUG CREATE BEGGINING', this.entityC);
    this.rentalService.create(this.entityC).subscribe(
      (a: RentalDtoUpdate) => {
        this.messageValidation = "Le Rental a bien été créer";
        console.log(a);
        this.router.navigateByUrl('confirmation/'+a.id);
      }
    );
  }

  update() {
    this.rentalService.update(this.entityU).subscribe(
      (b: RentalDtoUpdate) => {
        this.messageValidation = "Le rental à bien été mis à jour"
      }
    );
  }

  readById() {
    this.rentalService.readById(this.id).subscribe(
      (c: RentalDto) => {
        this.entity = c;
      }

    );
  }

  readAll() {
    this.rentalService.readAll().subscribe(
      (d: Array<RentalDto>) => {
        this.listRentalAll = d;
      }

    );
  }

  readDelete() {
    this.rentalService.readDelete().subscribe(
      (e: Array<RentalDto>) => {
        this.listRentalDelete = e;
      }

    );
  }

  readAllReal() {
    this.rentalService.readAllReal().subscribe(
      (f: Array<RentalDto>) => {
        this.listRentalReal = f;
      }
    );
  }

  delete() {
    this.rentalService.delete(this.id).subscribe(
      (g: string) => {
        this.messageValidation = "Le Rental à bien été supprimé DEFINITEVEMENT";
      }

    );
  }

  setDeleteTrue() {
    this.rentalService.setDeleteTrue(this.id).subscribe(
      (h: string) => {
        this.messageValidation = "Le rental à bien été supprimé";
      }

    );
  }

  availability() {
    this.rentalService.availability(this.idDevice).subscribe(
      (i: Array<RentalDtoAvail>) => {
        this.listAvail = i;
      }

    );
  }

  addReview() {
    this.rentalService.addReview(this.idRental, this.idReview).subscribe(
      (j: string) => {
        this.messageValidation = "La Review à bien été ajouté au Rental";
      }

    );
  }

  addEndDateReal() {
    this.rentalService.addEndDateReal(this.idEnd).subscribe(
      (k: number) => {
        if (k == 0) {
          this.messageValidation = "Le Device à bien été rendu";
        } else {
          this.messageValidation = "Le Device à été rendu avec " + k + "h de retard";
        }
      }
    );
  }


}
