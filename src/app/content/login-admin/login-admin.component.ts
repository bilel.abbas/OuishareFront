import { Component, OnInit } from '@angular/core';
import { AdminService } from 'src/app/services/admin.service';
import { Router } from '@angular/router';
import { AdminDtoLogin } from 'src/app/models/admin-dto-login';

@Component({
  selector: 'app-login-admin',
  templateUrl: './login-admin.component.html',
  styleUrls: ['./login-admin.component.css']
})
export class LoginAdminComponent implements OnInit {

  entity : AdminDtoLogin = new AdminDtoLogin();
  message: string;
  messageColor: string;

  constructor(private service : AdminService, public router:Router) { }

  ngOnInit() {
    if(localStorage.getItem('isLoggedAdmin') =='true'){
      this.router.navigateByUrl('/');
    }
  }

  annuler(){
    this.entity.email="";
    this.entity.pwd="";
  }

  login() {
    console.log('DEBUG LOGINADMIN INPUTS : ', this.entity)
    this.service.login(this.entity).subscribe(
      (resp) => {
        console.log('DEBUG LOGINADMIN RESPONSE : ', resp)
        if(resp != null){
          this.messageColor = 'green';
          this.message = 'Le client dont l\'email est ' + this.entity.email+' est correct'; 
          localStorage.setItem('isLoggedAdmin', 'true');

          location.reload();
        }else{
          this.messageColor = 'red';
           this.message = 'Le client n\'existe pas!';
        }


      },
    (err) => {
     this.messageColor = 'red';
      this.message = 'Le client n\'existe pas!';
      console.log('DEBUG ERROR : ', err); 

    
     if (err.error.status === 400) {
        this.message += ' Les champs ne sont pas valides!';
     }
      

     if(this.entity.email ==null || this.entity.pwd==null) {
      if((this.entity.email==null) && (this.entity.pwd==null)){     
        this.message ='Veuillez mettre un email et un mot de passe';
      }
  
      if((this.entity.email==null) && (this.entity.pwd!=null)){   

        this.message ='Veuillez mettre un email';
      }

      if((this.entity.email!=null) && (this.entity.pwd==null)){   

        this.message='Veuillez mettre un mot de passe';
      }
    }
  }
  );
  }

  
}
