import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BestDevicesComponent } from './best-devices.component';

describe('BestDevicesComponent', () => {
  let component: BestDevicesComponent;
  let fixture: ComponentFixture<BestDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BestDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BestDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
