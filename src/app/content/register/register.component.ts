import { Component, OnInit } from '@angular/core';
import { CustomerDtoCreate } from 'src/app/models/customer-dto-create';
import { CustomerService } from 'src/app/services/customer.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  
  entity : CustomerDtoCreate = new CustomerDtoCreate();
  message: string;
  messageColor: string;
  formulaire : FormGroup;

  constructor(private service : CustomerService, public router: Router) { }

  ngOnInit() {
    this.formulaire = new FormGroup ({
      email : new FormControl('',Validators.required),
      lastName :  new FormControl('',Validators.required),
      firstName : new FormControl('',Validators.required),
      adress :  new FormControl('',Validators.required),
      zipCode : new FormControl('',Validators.required),
      city :  new FormControl('',Validators.required),
      pwd : new FormControl('',Validators.required),
      phone : new FormControl('',Validators.required)
    });
  }

  annuler(){
    this.entity.email="";
    this.entity.lastName="";
    this.entity.firstName="";
    this.entity.adress="";
    this.entity.zipCode=0;
    this.entity.city="";
    this.entity.pwd="";
    this.entity.phone="";
  }


register() {
  this.service.register(this.entity).subscribe(
    (success) => {
      this.messageColor = 'green';
      this.message = 'Le client ' + this.entity.firstName+' '+this.entity.lastName + ' a été ajouté avec succès :)';
      this.router.navigateByUrl('/login');
    },
  (err) => {
    this.messageColor = 'red';
    this.message = 'Le client n\'a pas été ajouté!';
    console.log('DEBUG ERROR : ', err);
    if (err.error.status === 400) {
      this.message += ' Les champs ne sont pas valides!';
    }
  }
);
}
}
