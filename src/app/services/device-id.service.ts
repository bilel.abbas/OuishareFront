import { Injectable } from '@angular/core';
import { Device } from '../models/device';
import { HttpClient }    from '@angular/common/http';
import {environment} from '../../environments/environment';
import { RentalComponent } from '../content/rental/rental.component';
@Injectable({
  providedIn: 'root'
})
export class DeviceIdService {

  url ='http://localhost:8080/api/device/';
  constructor(private http: HttpClient) { }


  searchById(idDevice: number){
    return this.http.get(this.url+idDevice+'/');
  }
}
