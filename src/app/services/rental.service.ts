import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RentalDtoCreate } from '../models/rental-dto-create';
import { RentalDtoUpdate } from '../models/rental-dto-update';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RentalService {

  url = environment.BASE_URL +'rental/';

  constructor(private http: HttpClient) { }

  create(entity: RentalDtoCreate) {
    return this.http.post(this.url, entity);
  }

  update(entity: RentalDtoUpdate) {
    return this.http.put(this.url, entity);
  }

  readById(id: number) {
    return this.http.get(this.url + id + '/');
  }

  readAll() {
    return this.http.get(this.url + 'read-all/');
  }

  readDelete() {
    return this.http.get(this.url + 'read-deleted/');
  }

  readAllReal() {
    return this.http.get(this.url + 'read-all-real/');
  }

  delete(id: number) {
    return this.http.delete(this.url + id + '/');
  }

  setDeleteTrue(id: number) {
    return this.http.put(this.url + id + '/', null);
  }

  availability(idDevice: number) {
    return this.http.get(this.url + idDevice + '/availability');
  }

  addReview(idRental: number, idReview: number) {
    return this.http.put(this.url + idRental + '/add-review/' + idReview + '/', null);
  }

  addEndDateReal(id: number) {
    return this.http.put(this.url + id + '/end/', null);
  }
}
