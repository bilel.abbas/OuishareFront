import { Injectable } from '@angular/core';
import { HttpClient }    from '@angular/common/http';
import {environment} from '../../environments/environment';
import { DeviceDto } from '../models/device-dto';
@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  url ='http://localhost:8080/api/device/';
  constructor(private http: HttpClient) { }

  create(dto : DeviceDto){
      return this.http.post(this.url,dto);
  }

 
}
