import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LockerDto } from '../models/lockerDto';

@Injectable({
  providedIn: 'root'
})
export class LockerService {

  private url = 'http://localhost:8080/api/locker/';

  constructor(private http: HttpClient) { }

  create(dto : LockerDto) {
    return this.http.post(this.url,dto);
  }
}