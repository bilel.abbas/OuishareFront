import { Injectable } from '@angular/core';
import { CustomerDtoCreate } from '../models/customer-dto-create';
import { CustomerDtoLogin } from '../models/customer-dto-login';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private url = 'http://localhost:8080/api/customer/';


  constructor(private http: HttpClient) { }

  register(dto : CustomerDtoCreate) {
    return this.http.post(this.url,dto);
  }
  
  login(dto : CustomerDtoLogin){

    return this.http.post(this.url+'login/',dto); 
  }

  findByEmail(email:string){
    return this.http.get(this.url+'get-email/'+email+'/'); 

  }

  


}