import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AdminDtoLogin } from '../models/admin-dto-login';


@Injectable({
  providedIn: 'root'
})
export class AdminService {

  private url = 'http://localhost:8080/api/admin/';
  constructor(private http:HttpClient) { }

  
  
  login(dto : AdminDtoLogin){

    return this.http.post(this.url+'login/',dto); 
  }
}
