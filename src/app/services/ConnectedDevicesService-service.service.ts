import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ConnectedCustomerWithIdDto } from '../models/connected-customer-with-id-dto';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConnectedDevicesService {

  private urlReadAllCatalog = environment.BASE_URL+'device/read-all-catalog/';
  private urlReadAllInCustomer = environment.BASE_URL+'device/read_all_from_customer/';
  private urlReadAllSubsInCustomer = environment.BASE_URL+'device/read_all_subs_from_customer/';
  private urlReadAvailabilityNow=environment.BASE_URL+'rental/{id}/availability-now/';

  constructor(private http: HttpClient) { }

  readAllCatalog() {
    return this.http.get(this.urlReadAllCatalog);
  }

  readAllInCustomerFromCustomerId(idCustomer: string) {

    return this.http.get(this.urlReadAllInCustomer+idCustomer+'/');
  }

  readAllFromSubscribedFromCustomerId(idCustomer: string) {

    return this.http.get(this.urlReadAllSubsInCustomer+idCustomer+'/');
  }

  isAvailableNow(idDevice: number) {
 
    return this.http.get(environment.BASE_URL+'rental/'+String(idDevice)+'/availability-now/');
  }

}