export class CustomerDtoCreate {

    email : string;
    lastName : string;
    firstName: string;
    adress: string;
    zipCode : number;
    city: string;
    pwd: string;
    phone: string;

}
