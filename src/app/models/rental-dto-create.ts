export class RentalDtoCreate {
    startDate: any;
    endDateTheoritical: any;
    subscription: boolean;
    idDevice: number;
    idCustomer: number;
}
