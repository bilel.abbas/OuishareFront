export class RentalDtoUpdate {
    id: number;
    startDate: Date;
    endDateTheoritical: Date;
    subscription: boolean;
    idDevice: number;
    idCustomer: number;
}
