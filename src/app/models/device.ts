export class Device {
    id: number;
    name: string;
    brand: string;
    description: string;
    priceSub: number;
    pricePonctual: number;
    deposit: number;
    priceExtra: number;
    state: string;
    stateComment: string;
    chip: number;
    rentalDurationInHours: number;
    rules: string;
    urlImage: string;
}
