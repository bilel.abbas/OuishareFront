export class LockerDto {
    id: number;
    adress: string;
    zipCode: number;
    city: string;
    slot: number;
    name: string;
    deleted: boolean;
}