import { Device } from './device';
import { CustomerDtoUpdate } from './customer-dto-update';

export class RentalDto {
    id: number;
    deleted: boolean;
    startDate: Date;
    endDateTheoritical: Date;
    subscription: boolean;
    device: Device;
    customer: CustomerDtoUpdate;
}
