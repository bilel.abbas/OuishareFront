import { Component, OnInit } from '@angular/core';
import { Routes, Router } from '@angular/router';
import { LoginComponent } from '../content/login/login.component';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  
  isLogged = localStorage.getItem('isLogged');
  isLoggedAdmin = localStorage.getItem('isLoggedAdmin');
  
  constructor(public router:Router) { }

  ngOnInit() {
    
  }

  onLogin() {
    this.router.navigateByUrl('/login');
}

onRegister() {
  this.router.navigateByUrl('/register');
}

onClient() {
  this.router.navigateByUrl('/client/connected-devices');
}

clientNavBar() {
if ( this.router.url === '/'
  || this.router.url === '/#section0'
  || this.router.url === '/#section1'
  || this.router.url === '/#section2'
  || this.router.url === '/#section3'
  || this.router.url === '/#section4'
  || this.router.url === '/login'
  || this.router.url === '/register'
  || this.router.url === '/loginadmin'


)
return true;
else 
return false;


}

onDisconnect() {
  localStorage.removeItem('isLogged');

  localStorage.removeItem('loggedCustomer');
  
  window.location.reload();
  
}  
}
