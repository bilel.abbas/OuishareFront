import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { ConnectedDevicesComponent } from './content/connectedDevices/connected-devices/connected-devices.component';
import { MySubscriptionsComponent } from './content/connectedDevices/my-subscriptions/my-subscriptions.component';
import { DevicesInLockerComponent } from './content/connectedDevices/devices-in-locker/devices-in-locker.component';
import { CatalogComponent } from './content/connectedDevices/catalog/catalog.component';
import { HttpClientModule } from '@angular/common/http';

const appRoutes: Routes = [
  // { path: 'user-example', component: CrisisListComponent },
  // { path: 'user-example',      component: HeroDetailComponent },
  // { path: '**', component: PageNotFoundComponent }

];
import { RentalComponent } from './content/rental/rental.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { SearchComponent } from './search/search.component';
import { DashboardComponent } from './content/dashboard/dashboard.component';
import { OurServicesComponent } from './content/our-services/our-services.component';
import { AboutComponent } from './content/about/about.component';
import { AppRoutingModule } from './app-routing.module';
import { AllDevicesComponent } from './content/devices/all-devices/all-devices.component';
import { BestDevicesComponent } from './content/devices/best-devices/best-devices.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
//Si erreur de compile, tapez ca dans le terminal (SUR LE BON PROJET HEIN GUILLAUME??) : npm i --save angular-calendar date-fns
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CalendarComponent } from "./content/rental/calendar/calendar.component";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ValidationComponent } from "./content/validation/validation.component";
import { LoginComponent } from './content/login/login.component';
import { RegisterComponent } from './content/register/register.component';
import { ContactComponent } from './content/contact/contact.component';
import { PartenaireComponent } from './content/partenaire/partenaire.component';
import { ConceptComponent } from './content/concept/concept.component';
import { DeviceComponent } from './admin/device/device.component';
import { LockerComponent } from './admin/locker/locker.component';
import { AdminComponent } from './admin/admin.component';
import { DashboardAdminComponent} from './admin/dashboardAdmin/dashboardAdmin.component'
import { LoginAdminComponent } from './content/login-admin/login-admin.component';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData } from '@angular/common';
import { ConfirmationAbonnementComponent } from './content/confirmation-abonnement/confirmation-abonnement.component';

registerLocaleData(localeFr);


@NgModule({
  declarations: [
    ValidationComponent,
    AppComponent,
    RentalComponent,
    NavBarComponent,
    ContentComponent,
    FooterComponent,
    SearchComponent,
    DashboardComponent,
    OurServicesComponent,
    AboutComponent,
    AllDevicesComponent,
    BestDevicesComponent,
    ConnectedDevicesComponent,
    MySubscriptionsComponent,
    DevicesInLockerComponent,
    CatalogComponent,
    CalendarComponent,
    ValidationComponent,
    LoginComponent,
    RegisterComponent,
    ContactComponent,
    PartenaireComponent,
    ConceptComponent,
    DeviceComponent,
    LockerComponent,
    AdminComponent,
    DashboardAdminComponent,
    LoginAdminComponent,
    ConfirmationAbonnementComponent,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    AppRoutingModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory })
  ],
  providers: [
    LoginComponent
  ],
  exports: [CalendarComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
