import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './content/dashboard/dashboard.component';
import { AboutComponent } from './content/about/about.component';
import { OurServicesComponent } from './content/our-services/our-services.component';
import { ConnectedDevicesComponent } from './content/connectedDevices/connected-devices/connected-devices.component';
import { ContactComponent } from './content/contact/contact.component';
import { ConceptComponent } from './content/concept/concept.component';
import { BestDevicesComponent } from './content/devices/best-devices/best-devices.component';
import { PartenaireComponent } from './content/partenaire/partenaire.component';
import { RentalComponent } from './content/rental/rental.component';
import { AdminComponent } from './admin/admin.component';
import { LockerComponent } from './admin/locker/locker.component';
import { DeviceComponent } from './admin/device/device.component';
import { ValidationComponent } from './content/validation/validation.component';
import { AppComponent } from './app.component';
import { LoginComponent } from './content/login/login.component';
import { RegisterComponent } from './content/register/register.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginAdminComponent } from './content/login-admin/login-admin.component';
import { ConfirmationAbonnementComponent } from './content/confirmation-abonnement/confirmation-abonnement.component';


const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'about', component: AboutComponent },
  { path: 'our-services', component: OurServicesComponent },
  { path: 'confirmation-abonnement/:id', component: ConfirmationAbonnementComponent},
  { path: 'client/connected-devices', component: ConnectedDevicesComponent},
  {path: 'rental/:id', component: RentalComponent},
  {path: 'rental', component: RentalComponent},

  {path : 'contact', component : ContactComponent},
  {path : 'concept', component : ConceptComponent},
  {path : 'bestdevices', component: BestDevicesComponent},
  {path: 'partenaires', component : PartenaireComponent},
  {path: 'rental/:id', component: RentalComponent},
  {path : 'device', component : DeviceComponent},
  {path:'confirmation/:id', component:ValidationComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent },
  {path: 'admin', component: AdminComponent},
  {path: 'locker', component: LockerComponent},
  { path: 'loginadmin', component: LoginAdminComponent}


  // {path: '', component: }


];

@NgModule({
  imports: [
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes,
      { enableTracing: false } // <-- debugging purposes only
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
