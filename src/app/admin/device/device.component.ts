import { Component, OnInit } from '@angular/core';
import { DeviceIdService } from 'src/app/services/device-id.service';
import { Device } from 'src/app/models/device';
import { DeviceDto } from 'src/app/models/device-dto';
import { DeviceService } from 'src/app/services/device-service.service';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {

  entity : DeviceDto = new DeviceDto();
  message: string;
  messageColor: string;

  constructor(private service : DeviceService) { }

  ngOnInit() {
  }


  annuler(){
    this.entity.name="";
    this.entity.brand="";
    this.entity.chip=null;
    this.entity.deposit=null;
    this.entity.description="";
    this.entity.priceExtra=null;
    this.entity.pricePonctual=null;
    this.entity.priceSub=null;
    this.entity.rentalDurationInHours=null;
    this.entity.rules="";
    this.entity.state="";
    this.entity.stateComment="";
    this.entity.urlImage="";
  }

  create(){
    this.service.create(this.entity).subscribe(
      (response) => {
        this.messageColor = 'green';
        this.message = "L'objet " + this.entity.name + ' a été ajouté avec succès';
        console.log(this.message);
      },
      (err) =>{
        this.messageColor = 'red';
        this.message = "L'objet n'a pas été ajouté";
        console.log('DEBUG ERROR : ', err);
        if (err.error.status === 400) {
          this.message += '. Les champs ne sont pas valides!';
        }
      }

    )

  }

}
