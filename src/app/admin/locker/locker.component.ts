import { Component, OnInit } from '@angular/core';
import { LockerDto } from 'src/app/models/lockerDto';
import { LockerService } from 'src/app/services/Locker-service';

@Component({
  selector: 'app-locker',
  templateUrl: './locker.component.html',
  styleUrls: ['./locker.component.css']
})
export class LockerComponent implements OnInit {

  entity: LockerDto = new LockerDto();
  message: string;
  messageColor: string;

  constructor(private service: LockerService) { }

  ngOnInit() {
  }

  annuler() {
    this.entity.adress = "";
    this.entity.zipCode = 0;
    this.entity.city = "";
    this.entity.name = "";
    this.entity.slot = 0;
  }

  create() {
    console.log('DEBUG ENTITY BEFORE SENDING HTTP REQUEST : ', this.entity)
    this.service.create(this.entity).subscribe(
      (response) => {
        console.log('DEBUG HTTP RESPONSE : ', response)
        if(response != null){
          //SUCCESS
        this.messageColor = "green";
        this.message = 'le Locker ' + this.entity.name + " a été créé à l'adresse suivante : " + this.entity.adress;
        console.log(this.message)
        }else{
          //ALREADY EXIST
        this.messageColor = "orange";
        this.message = 'le Locker ' + this.entity.name + " existe déjà dans la BD " + this.entity.adress;
        console.log(this.message)
        }
      },
      (err) => {
        this.messageColor = "red";
        this.message = "le locker n'a pas été ajouté !";
        console.log('DEBUG ERROR : ', err);
        if (err.error.status === 400) {
          this.message += ' Les champs ne sont pas valides!';
        }
      }
    )
  }
}
