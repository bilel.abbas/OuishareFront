﻿-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 04 déc. 2019 à 09:50
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `oui_share_db`
--
CREATE DATABASE IF NOT EXISTS `oui_share_db` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `oui_share_db`;

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_c0r9atamxvbhjjvy5j8da1kam` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `customer`
--

DROP TABLE IF EXISTS `customer`;
CREATE TABLE IF NOT EXISTS `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `adress` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `id_locker` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_dwk6cx0afu8bs9o4t536v1j5v` (`email`),
  KEY `FKr0wa1brydvah3r63nosa3glbx` (`id_locker`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `customer`
--

INSERT INTO `customer` (`id`, `deleted`, `adress`, `city`, `email`, `first_name`, `last_name`, `phone`, `pwd`, `zip_code`, `id_locker`) VALUES
(1, b'0', '1 rue des pisselits', 'Chaille-les-oies', 'r@c.fr', 'Jacques', 'Chirac', '699254917', 'pwd0126464131', 38330, 1),
(2, b'0', '1 rue des pisselits', 'Chaille-les-oies', 'c@c.fr', 'REné', 'Coty', '699254917', 'pwd0126464131', 38330, 2);

-- --------------------------------------------------------

--
-- Structure de la table `device`
--

DROP TABLE IF EXISTS `device`;
CREATE TABLE IF NOT EXISTS `device` (
  `id` bigint(20) NOT NULL,
  `deleted` bit(1) NOT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `chip` bigint(20) NOT NULL,
  `deposit` double NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `price_extra` double NOT NULL,
  `price_ponctual` double NOT NULL,
  `price_sub` double NOT NULL,
  `rental_duration_in_hours` double NOT NULL,
  `rules` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `state_comment` varchar(255) DEFAULT NULL,
  `url_image` varchar(255) DEFAULT NULL,
  `id_slot` bigint(20) DEFAULT NULL,
  `id_type` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKpdubvgl0fxixrrv8ed2odkxjp` (`id_slot`),
  KEY `FKbtcauwr3l109b9ty0aqkkly9h` (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `device`
--

INSERT INTO `device` (`id`, `deleted`, `brand`, `chip`, `deposit`, `description`, `name`, `price_extra`, `price_ponctual`, `price_sub`, `rental_duration_in_hours`, `rules`, `state`, `state_comment`, `url_image`, `id_slot`, `id_type`) VALUES
(21120, b'0', 'dyson', 111111, 150, 'aspirateur trop style', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', NULL, 1, NULL),
(21121, b'0', 'rowenta', 111111, 150, 'aspirateur moins style', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', NULL, 2, NULL),
(21122, b'0', 'miele', 111111, 150, 'aspirateur pas du tout style', 'aspirateur', 15, 15, 15, 1, 'Pas de règles', 'Bon état', 'Rien à signaler', NULL, 3, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Structure de la table `locker`
--

DROP TABLE IF EXISTS `locker`;
CREATE TABLE IF NOT EXISTS `locker` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `adress` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `slot` bigint(20) DEFAULT NULL,
  `zip_code` bigint(20) DEFAULT NULL,
  `id_admin` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_3d1vptsth5uevuncee6vc4lww` (`name`),
  KEY `FKio1fx8wvijux8rba0ohw6k7j4` (`id_admin`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `locker`
--

INSERT INTO `locker` (`id`, `deleted`, `adress`, `city`, `name`, `slot`, `zip_code`, `id_admin`) VALUES
(1, b'0', 'adress1', 'lyon', 'pierre', 123, 123456789, NULL),
(2, b'0', 'adress1', 'lyon', 'jacques', 123, 123456789, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `rental`
--

DROP TABLE IF EXISTS `rental`;
CREATE TABLE IF NOT EXISTS `rental` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `end_date_real` datetime(6) DEFAULT NULL,
  `end_date_theoritical` datetime(6) NOT NULL,
  `start_date` datetime(6) NOT NULL,
  `subscription` bit(1) NOT NULL,
  `customer_id` bigint(20) NOT NULL,
  `device_id` bigint(20) NOT NULL,
  `review_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK7jf1f7b8w6jotl5qw1kxn8114` (`customer_id`),
  KEY `FK4bfo3lp2wy4sbg87k3ard85pl` (`device_id`),
  KEY `FK25fet0ev1eqr7o4x0e27o82by` (`review_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `review`
--

DROP TABLE IF EXISTS `review`;
CREATE TABLE IF NOT EXISTS `review` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `rating` int(11) NOT NULL,
  `rental_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKepgoywl64bq50e5ovpx0pduvf` (`rental_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `slot`
--

DROP TABLE IF EXISTS `slot`;
CREATE TABLE IF NOT EXISTS `slot` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `dimensions` varchar(255) DEFAULT NULL,
  `number` bigint(20) NOT NULL,
  `id_device` bigint(20) DEFAULT NULL,
  `id_locker` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKplb3irqepiekqjey0ni0298u5` (`id_device`),
  KEY `FK25c5smr2sgo1q54ljjwnvh3fr` (`id_locker`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `slot`
--

INSERT INTO `slot` (`id`, `deleted`, `dimensions`, `number`, `id_device`, `id_locker`) VALUES
(1, b'0', NULL, 123, 21120, 1),
(2, b'0', NULL, 223, 21121, 1),
(3, b'0', NULL, 323, 21122, 2);

-- --------------------------------------------------------

--
-- Structure de la table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
CREATE TABLE IF NOT EXISTS `subscription` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `end_date` date DEFAULT NULL,
  `renew` bit(1) NOT NULL,
  `start_date` date NOT NULL,
  `id_customer` bigint(20) DEFAULT NULL,
  `id_device` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK54nv0a6x1usyta2kqm3e8u950` (`id_customer`),
  KEY `FKk3p82ju043xitkmv7padrygqm` (`id_device`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_3tg65hx29l2ser69ddfwvhy4h` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user_example`
--

DROP TABLE IF EXISTS `user_example`;
CREATE TABLE IF NOT EXISTS `user_example` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `deleted` bit(1) NOT NULL,
  `date` datetime(6) DEFAULT NULL,
  `date_local` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_25arl06ulxxhqc5ihfuy2a4dm` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `FKr0wa1brydvah3r63nosa3glbx` FOREIGN KEY (`id_locker`) REFERENCES `locker` (`id`);

--
-- Contraintes pour la table `device`
--
ALTER TABLE `device`
  ADD CONSTRAINT `FKbtcauwr3l109b9ty0aqkkly9h` FOREIGN KEY (`id_type`) REFERENCES `type` (`id`),
  ADD CONSTRAINT `FKpdubvgl0fxixrrv8ed2odkxjp` FOREIGN KEY (`id_slot`) REFERENCES `slot` (`id`);

--
-- Contraintes pour la table `locker`
--
ALTER TABLE `locker`
  ADD CONSTRAINT `FKio1fx8wvijux8rba0ohw6k7j4` FOREIGN KEY (`id_admin`) REFERENCES `admin` (`id`);

--
-- Contraintes pour la table `rental`
--
ALTER TABLE `rental`
  ADD CONSTRAINT `FK25fet0ev1eqr7o4x0e27o82by` FOREIGN KEY (`review_id`) REFERENCES `review` (`id`),
  ADD CONSTRAINT `FK4bfo3lp2wy4sbg87k3ard85pl` FOREIGN KEY (`device_id`) REFERENCES `device` (`id`),
  ADD CONSTRAINT `FK7jf1f7b8w6jotl5qw1kxn8114` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Contraintes pour la table `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `FKepgoywl64bq50e5ovpx0pduvf` FOREIGN KEY (`rental_id`) REFERENCES `rental` (`id`);

--
-- Contraintes pour la table `slot`
--
ALTER TABLE `slot`
  ADD CONSTRAINT `FK25c5smr2sgo1q54ljjwnvh3fr` FOREIGN KEY (`id_locker`) REFERENCES `locker` (`id`),
  ADD CONSTRAINT `FKplb3irqepiekqjey0ni0298u5` FOREIGN KEY (`id_device`) REFERENCES `device` (`id`);

--
-- Contraintes pour la table `subscription`
--
ALTER TABLE `subscription`
  ADD CONSTRAINT `FK54nv0a6x1usyta2kqm3e8u950` FOREIGN KEY (`id_customer`) REFERENCES `customer` (`id`),
  ADD CONSTRAINT `FKk3p82ju043xitkmv7padrygqm` FOREIGN KEY (`id_device`) REFERENCES `device` (`id`);
--
-- Base de données :  `project_immobilier`
--
CREATE DATABASE IF NOT EXISTS `project_immobilier` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `project_immobilier`;

-- --------------------------------------------------------

--
-- Structure de la table `agent`
--

DROP TABLE IF EXISTS `agent`;
CREATE TABLE IF NOT EXISTS `agent` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `date_recrutement` date DEFAULT NULL,
  `pwd` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_pxogqxl64ae07j2lox1tgvrlx` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `bien`
--

DROP TABLE IF EXISTS `bien`;
CREATE TABLE IF NOT EXISTS `bien` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `prix` int(11) NOT NULL,
  `vendu` bit(1) NOT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKf0beayhg2qil8lqb65ptpucap` (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `type` varchar(255) NOT NULL,
  `agent_attribue_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_bfgjs3fem0hmjhvih80158x29` (`email`),
  KEY `FK1d9515th7bhcqimwba1m8j5h0` (`agent_attribue_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1),
(1);
--
-- Base de données :  `projet_spring`
--
CREATE DATABASE IF NOT EXISTS `projet_spring` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `projet_spring`;

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id` bigint(20) NOT NULL,
  `date_insertion` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  `date_suppression` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `id_societe` bigint(20) DEFAULT NULL,
  `domaine` varchar(255) DEFAULT NULL,
  `nom_entreprise` varchar(255) DEFAULT NULL,
  `nom_representant` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_bfgjs3fem0hmjhvih80158x29` (`email`),
  KEY `FK_4ima9uj4wnnlxo0rvxvd4cae` (`id_societe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id`, `date_insertion`, `date_modification`, `date_suppression`, `deleted`, `email`, `nom`, `prenom`, `id_societe`, `domaine`, `nom_entreprise`, `nom_representant`) VALUES
(4, '2019-11-14 09:44:54', NULL, NULL, b'0', 'admin@adaming.fr', 'a', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `contrat`
--

DROP TABLE IF EXISTS `contrat`;
CREATE TABLE IF NOT EXISTS `contrat` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_insertion` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  `date_suppression` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `date` date DEFAULT NULL,
  `methodologie` int(11) DEFAULT NULL,
  `id_societe` bigint(20) DEFAULT NULL,
  `id_client` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKp61a5d9ectumpsavo6j8r4nbm` (`id_societe`),
  KEY `FKba0kmopqb42xgef1al76k848e` (`id_client`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `employe`
--

DROP TABLE IF EXISTS `employe`;
CREATE TABLE IF NOT EXISTS `employe` (
  `id` bigint(20) NOT NULL,
  `date_insertion` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  `date_suppression` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `id_societe` bigint(20) DEFAULT NULL,
  `date_recrutement` date DEFAULT NULL,
  `fonction` int(11) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  `salaire` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_13xeyi3ex32ti4t6i4sotp28i` (`email`),
  KEY `FK_kjivmadelv8tejbdg041c1ti8` (`id_societe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `employe_projets`
--

DROP TABLE IF EXISTS `employe_projets`;
CREATE TABLE IF NOT EXISTS `employe_projets` (
  `employes_id` int(11) NOT NULL,
  `projets_id` int(11) NOT NULL,
  KEY `FKvyxhvub7ajbanoaeyn1p2w29` (`projets_id`),
  KEY `FKhyvjytxpc4w47m03g2j6x8g6n` (`employes_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(5);

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_insertion` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  `date_suppression` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `nom_equipe` varchar(255) DEFAULT NULL,
  `nom_projet` varchar(255) DEFAULT NULL,
  `id_client` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKhquklcvgo5vxr5mr8bj1g6ln8` (`id_client`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `projet_employe`
--

DROP TABLE IF EXISTS `projet_employe`;
CREATE TABLE IF NOT EXISTS `projet_employe` (
  `id_projet` bigint(20) NOT NULL,
  `id_employe` bigint(20) NOT NULL,
  KEY `FKjndomg195vroek0vjehvtv2s1` (`id_employe`),
  KEY `FKyg03r60keg38scmgl4bj8ewi` (`id_projet`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `societe`
--

DROP TABLE IF EXISTS `societe`;
CREATE TABLE IF NOT EXISTS `societe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date_insertion` datetime DEFAULT NULL,
  `date_modification` datetime DEFAULT NULL,
  `date_suppression` datetime DEFAULT NULL,
  `deleted` bit(1) NOT NULL,
  `date_creation` date DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `nom_societe` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
--
-- Base de données :  `projet_web_dyn`
--
CREATE DATABASE IF NOT EXISTS `projet_web_dyn` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `projet_web_dyn`;

-- --------------------------------------------------------

--
-- Structure de la table `admin`
--

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `email` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pwd` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `admin`
--

INSERT INTO `admin` (`email`, `name`, `pwd`) VALUES
('aureljournet@orange.fr', 'Journet', 'test'),
('toto.fr', 'Toto', 'test');

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `imagePath` varchar(255) DEFAULT NULL,
  `price` double NOT NULL,
  `productName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `category`, `description`, `imagePath`, `price`, `productName`) VALUES
(1, 'Alimentation', 'Pizza au pepperoni', 'https://www.atelierdeschefs.com/media/recette-e30299-pizza-pepperoni-tomate-mozza.jpg', 2.5, 'Pizza'),
(4, 'High-Tech', 'C\'est un ordinateur', 'https://static.fnac-static.com/multimedia/Images/4B/4B/96/72/7509579-1505-1540-1/tsp20180208182843/Ordinateur-Portable-Jumper-EZbook-2-14-1-1920-1080-Intel-Cherry-trail-Z8350-Windows10-4-Go-64-Go.jpg', 299, 'Ordinateur'),
(5, 'T?l?phonie', 'T?l?phone de type nokia', 'https://images.fr.shopping.rakuten.com/photo/nokia-3310-1028270070_ML.jpg', 99.99, 'T?l?phone portable'),
(6, 'Int?rieur', 'Lampe de bureau', 'https://images-na.ssl-images-amazon.com/images/I/61%2Bl5qWUk7L._SX466_.jpg', 59, 'Lampe');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
